// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function string2(str){
    if(!str)        //for null, undefined, emplty string values
        return [];
    let arr=str.split('.').map((num)=>{ //converting each string value of the array to number.
        return Number(num);         //if any non integer is encountered, NaN will be returned and added to the array
    });
    if(arr.includes(NaN) || arr.length!=4)  //if non integer values || if IPv4 IP address not found  
        return [];
    else
        return arr;
}
module.exports = string2;