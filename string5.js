// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

function string5(arr){

    if(!arr)        //for null, undefined, empty values
    return '';
    let res=arr.map((str)=>{
        return String(str).trim();     //casting all elements into string format and removing any extra spaces
    });
    res=res.filter(str => str); //removes all empty values

    return res.toString().replace('[','').replace(']','').replace(/,/g,' '); //removes the bracket and commas from converted array (to String).

}
module.exports = string5;
