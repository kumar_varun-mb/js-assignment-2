// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function string4(obj){

    if(!obj)        //for null, undefined, empty values
    return 'Please re-enter name.';

    const arr=Object.values(obj).map((str)=>{
        str=String(str).toLowerCase();      
        return str.replace(str[0],str[0].toUpperCase());   //converting 1st letter of each word to caps
    });
    return arr.toString().replace('[','').replace(']','').replace(/,/g,' ');    //array to string
}
module.exports = string4;