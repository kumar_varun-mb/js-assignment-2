// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

const months=['January','February','March','April','May','June','July','August','September','October','November','December'];
function string3(str){
    if(!str)        //for null, undefined, emplty string values
        return 'Invalid Entry';
    const arr=str.split('/');
    if((arr[2].length!=4) || (arr[1]<1 || arr[1]>12) || (arr[0]<1 && arr[0]>31))
        return 'Invalid Entry';
        
    let res=months[arr[1]-1];
    
    if((res=='April'||res=='June'||res=='September'||res=='November') && arr[0]>30)
         return 'Invalid Entry'; 
    if(res=='February' && arr[0]>28)
        return 'Invalid Entry'; 

    return res;
}
module.exports = string3;