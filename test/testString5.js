const string5=require('../string5.js');

console.log(string5(["the", "quick", "brown", "fox"]));
console.log(string5(["my name is", "khan"]));
console.log(string5([]));
console.log(string5(undefined));
console.log(string5(null));
console.log(string5(["James", "Bond", " ", "007"]));
console.log(string5(["James   ", "    Bond", "0-0-7", " ", "008"]));
