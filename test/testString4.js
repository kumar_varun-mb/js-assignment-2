const string4=require('../string4.js');

console.log(string4({"first_name": "JoHN", "last_name": "SMith"}));
console.log(string4({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}));
console.log(string4({"first_name": "jaMes", "middle_name": "BOND", "last_name": "007"}));
console.log(string4(undefined));
console.log(string4(null));
console.log(string4(''));